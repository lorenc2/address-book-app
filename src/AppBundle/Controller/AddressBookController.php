<?php
// src/AppBundle/Controller/AddressBookController.php

namespace AppBundle\Controller;

use AppBundle\Entity\AddressBook;
use AppBundle\Form\AddressBookType;
use AppBundle\Services\AddressBookManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AddressBookController extends Controller
{
    /**
     * @Route("/", name="addressBookList")
     * @param AddressBookManager $addressBookManager
     * @return Response
     */
    public function indexAction(AddressBookManager $addressBookManager)
    {
        $addressBooks = $addressBookManager->getAllAddressBook();

        return $this->render('address_book/index.html.twig', array(
            'addressBooks' => $addressBooks
        ));
    }

    /**
     * @Route("/create", name="addressBookCreate")
     * @param Request $request
     * @param AddressBookManager $addressBookManager
     * @return Response
     */
    public function createAction(Request $request, AddressBookManager $addressBookManager)
    {
        $form = $this->createForm(AddressBookType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var AddressBook $addressBook */
            $addressBook = $form->getData();
            $addressBookManager->createAddressBook($addressBook);

            $this->addFlash('success', 'Address Book added successfully!');
            return $this->redirectToRoute('addressBookList');

        } elseif ($form->isSubmitted() && $form->isValid() === false) {
            $this->addFlash('error', 'Please enter valid data');
        }
        return $this->render('address_book/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}", name="addressBookEdit")
     *
     * @param int $id
     * @param Request $request
     *
     * @param AddressBookManager $addressBookManager
     * @return Response
     */
    public function editAction($id, Request $request, AddressBookManager $addressBookManager)
    {
        /** @var AddressBook $addressBook */
        $addressBook = $addressBookManager->getAddressBook($id);
        if (empty($addressBook)) {
            $this->addFlash('error', 'Address Book Not Found');
            return $this->redirectToRoute('addressBookList');
        }
        $form = $this->createForm(AddressBookType::class, $addressBook);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var AddressBook $addressBook */
            $addressBook = $form->getData();
            $addressBookManager->updateAddressBook($id, $addressBook);

            $this->addFlash(
                'notice',
                'Address Updated!'
            );

            return $this->redirectToRoute('addressBookList');
        }

        return $this->render('address_book/edit.html.twig', array(
            'addressBook' => $addressBook,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/show/{id}", name="addressBookShow")
     * @param int $id
     * @param AddressBookManager $addressBookManager
     * @return Response
     */
    public function showAction($id, AddressBookManager $addressBookManager)
    {
        $addressBook = $addressBookManager->getAddressBook($id);
        if (empty($addressBook)) {
            $this->addFlash('error', 'Address Book Not Found');
            return $this->redirectToRoute('addressBookList');
        }

        return $this->render('address_book/show.html.twig', array(
            'addressBook' => $addressBook
        ));
    }

    /**
     * @Route("/delete/{id}", name="addressBookDelete")
     * @param int $id
     * @param AddressBookManager $addressBookManager
     * @return RedirectResponse
     */
    public function deleteAction($id, AddressBookManager $addressBookManager)
    {
        $addressBookManager->deleteAddressBook($id);
        $this->addFlash(
            'notice',
            'Address Removed!'
        );
        return $this->redirectToRoute('addressBookList');
    }

}