<?php
// src/AppBundle/Form/AddressBookType.php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressBookType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class,
                array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('lastname', TextType::class,
                array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('street', TextType::class,
                array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('zip', NumberType::class,
                array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('city', TextType::class,
                array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('country', CountryType::class,
                array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('phonenumber', TextType::class,
                array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('birthday', BirthdayType::class,
                array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('email', EmailType::class,
                array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('save', SubmitType::class, [
                'attr' => ['class' => 'save btn btn-primary'],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\AddressBook'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_addressbook';
    }

}
