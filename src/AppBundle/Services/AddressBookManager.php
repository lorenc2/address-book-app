<?php
// src/AppBundle/Services/AddressBookManager.php

namespace AppBundle\Services;

use AppBundle\Entity\AddressBook;
use Doctrine\Common\Persistence\ObjectManager;

class AddressBookManager
{
    /** @var ObjectManager $manager */
    private $manager;

    /**
     * @param ObjectManager $manager
     */
    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param AddressBook $addressBookItem
     */
    public function createAddressBook(AddressBook $addressBookItem)
    {
        /** @var AddressBook $addressBook */
        $addressBook = new AddressBook();

        $addressBook->setFirstname($addressBookItem->getFirstname());
        $addressBook->setLastname($addressBookItem->getLastname());
        $addressBook->setEmail($addressBookItem->getEmail());
        $addressBook->setStreet($addressBookItem->getStreet());
        $addressBook->setZip($addressBookItem->getZip());
        $addressBook->setCity($addressBookItem->getCity());
        $addressBook->setCountry($addressBookItem->getCountry());
        $addressBook->setPhonenumber($addressBookItem->getPhonenumber());
        $addressBook->setBirthday($addressBookItem->getBirthday());

        $this->manager->persist($addressBook);
        $this->manager->flush();
    }

    /**
     * @param int $addressBookId
     * @param AddressBook $addressBookItem
     */
    public function updateAddressBook($addressBookId, AddressBook $addressBookItem)
    {

        /** @var AddressBook $addressBook */
        $addressBook = $this->getAddressBook($addressBookId);
        $addressBook->setFirstname($addressBookItem->getFirstname());
        $addressBook->setLastname($addressBookItem->getLastname());
        $addressBook->setEmail($addressBookItem->getEmail());
        $addressBook->setStreet($addressBookItem->getStreet());
        $addressBook->setZip($addressBookItem->getZip());
        $addressBook->setCity($addressBookItem->getCity());
        $addressBook->setCountry($addressBookItem->getCountry());
        $addressBook->setPhonenumber($addressBookItem->getPhonenumber());
        $addressBook->setBirthday($addressBookItem->getBirthday());

        $this->manager->persist($addressBook);
        $this->manager->flush();
    }

    /**
     * @param int $addressBookId
     * @return AddressBook|object
     */
    public function getAddressBook($addressBookId)
    {
        return $this->manager->find('AppBundle:AddressBook', $addressBookId);
    }

    /**
     * @return mixed
     */
    public function getAllAddressBook()
    {
        $addressBooks = $this->manager->getRepository('AppBundle:AddressBook')->findAll();
        return $addressBooks;
    }

    /**
     * @param int $addressBookId
     */
    public function deleteAddressBook($addressBookId)
    {
        /** @var AddressBook $addressBook */
        $addressBook = $this->getAddressBook($addressBookId);

        if ($addressBook != NULL) {
            $this->manager->remove($addressBook);
            $this->manager->flush();
        }
    }
}