<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\AddressBook;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use DateTime;

class AddressBookFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 8; $i++) {
            $addressBook = new AddressBook();
            $addressBook->setFirstname('firstname'.$i);
            $addressBook->setLastname('lastname'.$i);
            $addressBook->setEmail('email'.$i.'@email.com');
            $addressBook->setStreet('street'.$i);
            $addressBook->setZip('3333'.$i);
            $addressBook->setCity('city'.$i);
            $addressBook->setCountry('DE');
            $addressBook->setPhonenumber('4444'.$i);
            $addressBook->setBirthday(new DateTime('1993-01-0'.$i));
            $manager->persist($addressBook);
        }
        $manager->flush();
    }
}