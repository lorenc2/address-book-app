Address Book app
========================

**Installing and configuring**

1. Install composer globally

    https://getcomposer.org/doc/00-intro.md#globally
    
    Run
    
    ```
    composer install

2. To add dummy data you should run

    ```
    php bin/console doctrine:fixtures:load